﻿using Entity_Framework.Entity;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Factories
{
    public static class PointFactorie
    {
        public static Point EntityToModele(this Points entity)
        {
            return new Point(entity.Longitude,entity.Latitude,entity.Timer,entity.Distance,entity.NGearASCII,entity.P_BreakF,entity.A_Steer,entity.R_Pedal,entity.G_Lon,entity.G_Lat,entity.V_Car);
        }


        public static Points ModeleToEntity(this Point modele)
        {
            return new Points
            {
                Longitude = modele.Longitude,
                Latitude = modele.Latitude,
                Timer = modele.Timer,
                Distance = modele.Distance,
                NGearASCII = modele.NGear,
                P_BreakF = modele.PBrakeF,
                A_Steer = modele.ASteer,
                R_Pedal = modele.RPedal,
                G_Lon = modele.GLong,
                G_Lat = modele.GLat,
                V_Car = modele.VCar,
            };
            
        }
    }
}
