﻿using Entity_Framework.Entity;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Factories
{
    public static class CircuitFactorie
    {
        public static Circuit EntityToModele(this Circuits entity)
        {
            return new Circuit(nameArg:entity.Name);
        }

        public static Circuits ModeleToEntity(this Circuit modele) 
        {
            Circuits circuit = new Circuits();
            circuit.Name = modele.name;
            return circuit;
        }
    }
}
