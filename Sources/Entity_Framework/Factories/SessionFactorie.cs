﻿using Entity_Framework.Entity;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Factories
{
    public static class SessionFactorie
    {
        public static Session EntityToModele(this Sessions entity)
        {
            string circuitName = entity.Circuit != null ? entity.Circuit.Name : "Inconnu !";
            string pilotePseudo = entity.Pilote != null ? entity.Pilote.Pseudo : "Inconnu !";

            return new Session(
                IdArg: entity.Id,
                dateArg: entity.Date,
                nameArg: entity.Name,
                typeArg: entity.Type,
                nameCircuitArg: circuitName,
                namePiloteArg: pilotePseudo
            );
        }


        public static Sessions ModeleToEntity(this Session modele) 
        {
            Sessions entity = new Sessions();

            entity.Name = modele.name;
            entity.Type = modele.type;
            entity.Date = modele.date;
            
            return entity;
        }
    }
}
