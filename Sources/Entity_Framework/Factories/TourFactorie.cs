﻿using Entity_Framework.Entity;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Factories
{
    public static class TourFactorie
    {
        public static Tour EntityToModele(this Tours entity) 
        {
            return new Tour(TempsArg:entity.Temps,IdSessionArg:entity.IdSession,NumeroArg:entity.Numero);
        }

        public static Tours ModeleToEntity(this Tour modele)
        {
            Tours tours = new Tours();

            tours.Temps = modele.Temps;
            tours.Numero = modele.Numero;
            return tours;

        }
    }
}
