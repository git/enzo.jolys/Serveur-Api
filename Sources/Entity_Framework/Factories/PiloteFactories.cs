﻿using Entity_Framework.Entity;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Factories
{
    public static class PiloteFactories
    {

        public static Pilotes ModeleToEntity(this Pilote piloteModele)
        {
            Pilotes pilote = new Pilotes();

            pilote.Pseudo = piloteModele.Pseudo;
            pilote.Email = piloteModele.Email;
            pilote.Password = piloteModele.Password;
            pilote.Image = piloteModele.Image;

            return pilote;
        }

        public static Pilote EntityToModele(this Pilotes piloteEntity)
        {
            return new Pilote(_Image:piloteEntity.Image, _Pseudo:piloteEntity.Pseudo,_Email:piloteEntity.Email,_Password:piloteEntity.Password);
        }
    }
}
