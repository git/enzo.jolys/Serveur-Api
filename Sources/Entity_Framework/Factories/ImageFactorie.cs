﻿using Entity_Framework.Entity;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Factories
{
    public static class ImageFactorie
    {
        public static Image EntityToModele(this Images entity)
        {
            return new Image(base64:entity.Base64);
        }
    }
}
