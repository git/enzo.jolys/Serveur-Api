﻿using Dto.Classe;
using Entity_Framework.Entity;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Factories
{
    public static class EcurieFactorie
    {

        public static Ecurie EntityToModele(this Ecuries dto)
        {
            return new Ecurie(dto.Image,dto.Name  );
        }

        public static Ecuries ModeleToEntity(this Ecurie ecurie) 
        {
            Ecuries entity = new Ecuries();
            entity.Name = ecurie.name;
            entity.Image = ecurie.Image;
            return entity;
        }
    }
}
