﻿using Entity_Framework.Entity.Relations;
using System;
using System.Buffers.Text;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Entity
{
    public class Pilotes
    {
        public int Id { get; set; }

        public string Pseudo { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string Image { get;set; }


        // --------------------  Relation -------------------- //

        // Ecurie liste
        //public int? IdEcurie { get; set; }
        //public Ecuries Ecurie { get; set; }
        public List<Relation_Pilote_Ecurie> Ecurie { get; set; }

        //Ecurie propriétaire
        public Ecuries EcuriePropriétaire { get; set; }

        // Courses 
        public List<Sessions> Sessions { get; set; }

        //Images
        //public Images Image { get; set; }

    }
}
