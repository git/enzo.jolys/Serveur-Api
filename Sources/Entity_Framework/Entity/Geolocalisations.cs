﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Entity
{
    // Attribut à redefinir

    public class Geolocalisations
    {
        public long Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        // --------------------  Relation -------------------- //
        // Points 
        //public Points Point { get; set; }
    }
}
