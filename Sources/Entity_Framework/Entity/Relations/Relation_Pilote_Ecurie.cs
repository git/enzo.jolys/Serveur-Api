﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Entity.Relations
{
    public class Relation_Pilote_Ecurie
    {
        public string Grade { get; set; }


        // ----------- Relation ----------------//
        public int IdPilote { get; set; }
        public Pilotes Pilote { get; set; }


        public int IdEcurie { get; set; }
        public Ecuries Ecurie { get; set; }
    }
}
