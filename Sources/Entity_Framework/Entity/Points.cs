﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Entity
{
    // Attributs à redefinir 
    public class Points
    {
        public long Id { get; set; }
        public float Timer { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public float Distance { get; set; }
        public float P_BreakF { get; set; }
        public float NGearASCII { get; set; }
        public float A_Steer { get; set;}
        public float R_Pedal { get; set;}
        public float G_Lon { get; set; }
        public float G_Lat { get; set; }
        public float V_Car { get; set;}

        // --------------------  Relation -------------------- //
        // Geolocalisation 
        //public long IdGeolocalisation { get; set;}
        //public Geolocalisations Geolocalisation { get; set;}

        // Tours
        public long IdTours { get; set;}
        public Tours Tours { get; set;}
    }
}
