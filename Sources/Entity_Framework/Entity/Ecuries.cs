﻿using Entity_Framework.Entity.Relations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Entity
{
    public class Ecuries
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Image { get;set; }

        // --------------------  Relation -------------------- //

        //Relation_Ecurie_Pilote
        public List<Relation_Pilote_Ecurie> PilotesRelation { get; set; }


        // Pilote propriétaire
        public int IdPiloteProprietaire { get; set; }
        public Pilotes PiloteProprietaire { get; set; }
    }
}
