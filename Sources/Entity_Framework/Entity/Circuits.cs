﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Entity
{
    public class Circuits
    {
        public int Id { get; set; }
        public string Name { get; set; }

        // --------------------  Relation -------------------- //
        //Courses
        public List<Sessions> Course { get; set; }

    }
}
