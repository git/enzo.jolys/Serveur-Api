﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Entity
{
    public class Tours
    {
        public long Id { get; set; }
        public string Temps { get; set; }
        public int Numero { get; set; }

        // --------------------  Relation -------------------- //
        // Points 
        public List<Points> Points { get; set; }

        // Courses 
        public long IdSession { get; set; }
        public Sessions Session { get; set; }

    }
}
