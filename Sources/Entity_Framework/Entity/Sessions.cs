﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Entity
{
    public class Sessions
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string? Type { get; set; }
        public DateTime Date { get; set; }

        // --------------------  Relation -------------------- //
        //Circuit 
        public int? IdCircuit { get; set; }
        public Circuits? Circuit { get; set; }

        // Pilotes 
        public int IdPilote { get; set; }
        public Pilotes Pilote { get; set; }

        //Tours 
        public List<Tours> Tours { get; set; }
    }
}
