﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework.Entity
{
    public class Images
    {
        public int Id { get; set; }
        public string Base64 { get; set; }

        // ----- Relation ------ //
        [Required]
        public int IdPilote { get; set; }
        [ForeignKey(nameof(IdPilote))]
        public Pilotes Pilote { get; set; }
    }
}
