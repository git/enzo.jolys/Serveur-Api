﻿using Dto.Classe;
using Dto.Factories;
using Interface;
using Microsoft.AspNetCore.Mvc;
using Modele.Classe;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Sessions : Controller
    {
        private readonly IApi data;

        public Sessions(IApi manager)
        {
            this.data = manager;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string pseudo)
        {
            IEnumerable<Session> sessions = (await data.GetSessionsByPilotes(pseudo));
            if (sessions == null)
            { 
                return NotFound(pseudo);
            }
            return Ok(sessions.Select(e => e.ModeleToDTO()));   
        }
    }
}
