﻿using Dto.Classe;
using Dto.Factories;
using Interface;
using Microsoft.AspNetCore.Mvc;
using Modele.Classe;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Circuits : Controller
    {
        private readonly IApi data;

        public Circuits(IApi manager)
        {
            this.data = manager;
        }
        [HttpGet]
        public async Task<IActionResult> Get(string name = "All")
        {
            if (name == "All")
            {
                return Ok((await data.GetCircuits()).Select(e => e.ModeleToDTO()));
            }

            Circuit circuit = await data.GetOneCircuit(name);
            if (circuit == null)
            {
                return BadRequest(name);
            }
            return Ok(circuit.ModeleToDTO());
                
        }

        [HttpPost]
        public async Task<IActionResult> Post(CircuitDTO circuitDto)
        {
            Circuit circuit = await data.AddCircuit(circuitDto.DTOToModele());

            if (circuit == null)
            {
                return BadRequest("Le pilote existe déjà !");
            }
            return Ok(circuit.ModeleToDTO());
        }
    }
}
