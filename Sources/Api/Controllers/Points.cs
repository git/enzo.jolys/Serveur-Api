﻿using Dto.Factories;
using Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Modele.Classe;

namespace Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class Points : Controller
    {
        private readonly IApi data;

        public Points(IApi manager)
        {
            this.data = manager;
        }

        [HttpGet]
        public async Task<IActionResult> Get(long IdTour)
        {
            IEnumerable<Point> points = (await data.GetPoints(IdTour));
            if (points == null)
            {
                return BadRequest();
            }
            return Ok(points.Select(e => e.ModeleToDTO()));

        }

    }
}
