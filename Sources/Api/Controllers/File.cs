﻿using Extraction_Donnees.Extraction_EXECL;
using Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Modele.Classe;

namespace Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class File : Controller
    {
        private readonly IApi data;

        public File(IApi manager)
        {
            this.data = manager;
        }

        [HttpPost]
        public async Task<IActionResult> PostExecl(IFormFile file, string pseudoPilote,string nameSession, string nameCircuit, string typeSession)
        {
            if ( file == null || file.Length == 0)
            {
                return BadRequest("File vide");
            }
            Session session;
            try
            {
                session = await Excel.ReadExcel(file);
                await data.AddSession(session,pseudoPilote, nameCircuit, nameSession,typeSession);
            }
            catch(Exception e) {
                return BadRequest(e.Message);
            }
            
            return Ok(session.Tours[0].Points[0]);
        }
    }
}
