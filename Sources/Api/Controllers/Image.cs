﻿using Dto.Factories;
using Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Modele.Classe;

namespace Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class Image : Controller
    {
        private readonly IApi data;

        public Image(IApi manager)
        {
            this.data = manager;
        }

        [HttpGet]
        public async Task<IActionResult> Get(int IdPilote) 
        {
            Modele.Classe.Image img = (await data.GetImage(IdPilote));
            if (img == null)
            {
                return BadRequest(IdPilote);
            }
            return Ok(img.ModeleToDTO());
        }
    }
}
