﻿using Dto.Factories;
using Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Modele.Classe;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Tours : Controller
    {
        private readonly IApi data;

        public Tours(IApi manager)
        {
            this.data = manager;
        }

        [HttpGet]
        public async Task<IActionResult> Get(long IdSession)
        {
            IEnumerable<Tour> tours = (await data.GetTours(IdSession));
            if (tours == null)
            {
                return BadRequest(IdSession);
            }
            return Ok(tours.Select(e => e.ModeleToDTO()));
        }
    }
}
