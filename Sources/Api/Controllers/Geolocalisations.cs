﻿using Dto.Factories;
using Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Modele.Classe;
/*
namespace Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class Geolocalisations : Controller
    {
        private readonly IApi data;

        public Geolocalisations(IApi manager)
        {
            this.data = manager;
        }

        [HttpGet]
        public async Task<IActionResult> Get(long IdPoint)
        {
            Geolocalisation geo = (await data.GetGeolocalisations(IdPoint));
            if (geo == null)
            {
                return BadRequest(IdPoint);
            }
            return Ok(geo.ModeleToDTO());
        }
    }
}
*/