﻿using Dto.Factories;
using Interface;
using Microsoft.AspNetCore.Mvc;
using Modele.Classe;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class FullSession : ControllerBase
    {
        private readonly IApi data;

        public FullSession(IApi manager)
        {
            this.data = manager;
        }

        // GET: api/<FullSession>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<Session> sessions = (await data.GetFullSessions());
            if (sessions == null)
            {
                return NotFound();
            }
            return Ok(sessions.Select(e => e.ModeleToDTO()));
        }
    }
}
