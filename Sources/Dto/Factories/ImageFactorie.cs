﻿using Dto.Classe;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto.Factories
{
    public static class ImageFactorie
    {
        public static ImageDTO ModeleToDTO(this Image modele)
        {
            ImageDTO dto  = new ImageDTO();
            dto.Base64 = modele.Base64;
            return dto;
        }
    }
}
