﻿using Dto.Classe;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto.Factories
{
    public static class EcurieFactorie
    {
        public static EcurieDTO ModeleToDTO(this Ecurie ecurie)
        {
            EcurieDTO ecurieDTO = new EcurieDTO();

            ecurieDTO.Name = ecurie.name;
            ecurieDTO.Owner = ecurie.Owner.ModeleToDTO();
            ecurieDTO.Members = ecurie.Members.Select( e => e.ModeleToDTO()).ToList();
            ecurieDTO.WaitingsMembers = ecurie.WaitingMembers.Select(e => e.ModeleToDTO()).ToList();
            ecurieDTO.Image = ecurie.Image;

            return ecurieDTO;
        }

        public static Ecurie DTOToModele(this EcurieDTO dto)
        {
            return new Ecurie(dto.Image,nameArg: dto.Name);
        }
    }
}
