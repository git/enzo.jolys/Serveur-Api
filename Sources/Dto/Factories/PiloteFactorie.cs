﻿using Dto.Classe;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto.Factories
{
    public static class PiloteFactorie
    {

        public static PiloteDTO ModeleToDTO(this Pilote pilote)
        {
            PiloteDTO piloteDTO = new PiloteDTO();

            piloteDTO.Pseudo = pilote.Pseudo;
            piloteDTO.Email = pilote.Email;
            piloteDTO.Image = pilote.Image;
            return piloteDTO;
        }

        public static Pilote DTOToModele(this PiloteDTO piloteDto) 
        {
            return new Pilote(_Pseudo:piloteDto.Pseudo,_Email:piloteDto.Email,_Password:"Inconnu !",_Image:piloteDto.Image);
        }
    }
}
