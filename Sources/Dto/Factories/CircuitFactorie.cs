﻿using Dto.Classe;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto.Factories
{
    public static class CircuitFactorie
    {
        public static CircuitDTO ModeleToDTO(this Circuit circuit)
        {
            CircuitDTO circuitDTO = new CircuitDTO();

            circuitDTO.Name = circuit.name;

            return circuitDTO;
        }

        public static Circuit DTOToModele(this CircuitDTO dto) 
        {
            return new Circuit(nameArg:dto.Name);
        }
    }
}
