﻿using Dto.Classe;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto.Factories
{
    public static class PointFactorie
    {
        public static PointDTO ModeleToDTO(this Point modele)
        {
            PointDTO dto = new PointDTO();

            dto.Distance = modele.Distance;
            dto.Latitude = modele.Latitude;
            dto.Longitude = modele.Longitude;
            dto.Timer = modele.Timer;
            dto.NGear = modele.NGear;
            dto.PBrakeF = modele.PBrakeF;
            dto.ASteer = modele.ASteer;
            dto.RPedal = modele.RPedal;
            dto.GLong = modele.GLong;
            dto.GLat = modele.GLat;
            dto.VCar = modele.VCar;


            return dto;
        }
    }
}
