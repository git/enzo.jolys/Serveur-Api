﻿using Dto.Classe;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto.Factories
{
    public static class SessionFactorie
    {
        public static SessionDTO ModeleToDTO(this Session session)
        {
            SessionDTO sessionDTO = new SessionDTO();

            sessionDTO.Name = session.name;
            sessionDTO.Type = session.type;
            sessionDTO.NamePilote = session.namePilote;
            sessionDTO.NameCircuit = session.nameCircuit;
            sessionDTO.Date = session.date;
            sessionDTO.Id = session.Id;
            sessionDTO.Tours = session.Tours.Select(e => e.ModeleToDTO()).ToList() ;

            return sessionDTO;
        }
    }
}
