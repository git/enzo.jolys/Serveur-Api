﻿using Dto.Classe;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto.Factories
{
    public static class TourFactorie
    {
        public static TourDTO ModeleToDTO(this Tour modele)
        {
            TourDTO tourDTO = new TourDTO();

            tourDTO.IdSession = modele.IdSession;
            tourDTO.Temps = modele.Temps;
            tourDTO.Numero = modele.Numero;
            tourDTO.Points = modele.Points.Select(e => e.ModeleToDTO()).ToList();

            return tourDTO;
        }
    }
}
