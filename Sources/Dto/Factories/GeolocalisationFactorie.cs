﻿using Dto.Classe;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto.Factories
{
    public static class GeolocalisationFactorie
    {
        public static GeolocalisationDTO ModeleToDTO(this Geolocalisation modele)
        {
            GeolocalisationDTO dto = new GeolocalisationDTO();
            dto.Longitude = modele.Longitude;
            dto.Latitude = modele.Latitude;
            return dto;
        }
    }
}
