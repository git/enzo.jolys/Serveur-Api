﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto.Classe
{
    public class EcurieDTO
    {
        public string Name { get; set; }
        public List<PiloteDTO> Members { get; set; }
        public List<PiloteDTO> WaitingsMembers { get; set; }
        public PiloteDTO Owner { get; set; }

        public string Image { get; set; }
    }
}
