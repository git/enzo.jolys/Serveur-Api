﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto.Classe
{
    public class ImageDTO
    {
        public string Base64 { get; set; }
    }
}
