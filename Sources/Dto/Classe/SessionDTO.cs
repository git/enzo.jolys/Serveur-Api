﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto.Classe
{
    public class SessionDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public DateTime Date { get; set; }
    
        public string NameCircuit { get; set; }

        public string NamePilote { set; get; }

        public List<TourDTO> Tours { get; set; }
    }
}
