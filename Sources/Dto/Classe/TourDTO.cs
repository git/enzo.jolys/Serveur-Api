﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto.Classe
{
    public class TourDTO
    {
        public string Temps { get; set; }
        public long IdSession { get; set; }
        public int Numero { get; set; }
        public List<PointDTO> Points { get; set; }
    }
}
