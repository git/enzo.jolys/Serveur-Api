﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto.Classe
{
    public class PointDTO
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public float Timer { get; set; }
        public float Distance { get; set; }
        public float NGear { get; set; }
        public float PBrakeF { get; set; }
        public float ASteer { get; set; }
        public float RPedal { get; set; }
        public float GLong { get; set; }
        public float GLat { get; set; }
        public float VCar { get; set; }
    }
}
