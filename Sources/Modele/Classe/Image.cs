﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modele.Classe
{
    public class Image
    {
        public string Base64 { get; set; }

        public Image(string base64)
        {
            Base64 = base64;
        }
    }
}
