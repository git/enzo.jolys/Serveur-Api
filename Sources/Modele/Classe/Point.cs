﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Modele.Classe
{
    public class Point
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public float Timer { get; set; }
        public float Distance { get; set; }
        public float NGear { get; set; }
        public float PBrakeF { get; set; }
        public float ASteer { get; set; }
        public float RPedal { get; set; }
        public float GLong { get; set; }
        public float GLat { get; set; }
        public float VCar { get; set; }

        public Point(double longitude,double latitude, float timer, float distance, float nGear, float pBrakeF, float aSteer, float rPedal, float gLong, float gLat, float vCar)
        {
            Latitude =  latitude;
            Longitude = longitude;
            Timer = timer;
            Distance = distance;
            NGear = nGear;
            PBrakeF = pBrakeF;
            ASteer = aSteer;
            RPedal = rPedal;
            GLong = gLong;
            GLat = gLat;
            VCar = vCar;
        }

    }
}
