﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modele.Classe
{
    public class Circuit
    {
        public string name { get; set; }

        public Circuit(string nameArg = "Inconnu !")
        {
            name = nameArg;
        }
    }
}
