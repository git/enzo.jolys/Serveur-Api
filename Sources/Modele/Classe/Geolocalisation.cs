﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modele.Classe
{
    public class Geolocalisation
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public Geolocalisation(double LatitudeArg,double LongitudeArg)
        {
            Latitude = LatitudeArg;
            Longitude = LongitudeArg;
        }
    }
}
