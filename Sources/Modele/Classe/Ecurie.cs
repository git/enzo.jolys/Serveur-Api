﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modele.Classe
{
    public class Ecurie
    {
        public string name { get; set; }
        public Pilote Owner { get; set; }
        public List<Pilote> Members { get; set; }
        public List<Pilote> WaitingMembers { get; set; }

        public string Image { get; set; }


        public Ecurie(string _image,string nameArg = "Inconnu !") 
        {
            name = nameArg;
            Image = _image;
        }

    }
}
