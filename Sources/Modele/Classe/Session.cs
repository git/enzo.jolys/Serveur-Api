﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modele.Classe
{
    public class Session
    {
        public long Id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public DateTime date { get; set; }

        public string nameCircuit { get; set; }

        public string namePilote { set; get; }


        public List<Tour> Tours { get; set; }

        public Session() 
        { 
            Tours = new List<Tour>();
        }
        public Session(long IdArg,DateTime dateArg,string nameArg = "Inconnu !",string typeArg = "Inconnu !",string nameCircuitArg = "Inconnu !",string namePiloteArg = "Inconnu !") 
        {
            name = nameArg;
            type = typeArg;
            nameCircuit = nameCircuitArg;
            namePilote = namePiloteArg;
            date = dateArg;
            Id = IdArg;
        }

        public Session(List<Tour> tours, long IdArg, DateTime dateArg, string nameArg = "Inconnu !", string typeArg = "Inconnu !", string nameCircuitArg = "Inconnu !", string namePiloteArg = "Inconnu !")
        {
            name = nameArg;
            type = typeArg;
            nameCircuit = nameCircuitArg;
            namePilote = namePiloteArg;
            date = dateArg;
            Id = IdArg;
            Tours = tours;
        }

        public override string ToString()
        {
            string sb = "";

            foreach (Tour tour in Tours)
            {
                sb += tour.ToString();
            }

            return sb;
        }
    }
}
