﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//classe tour
namespace Modele.Classe
{
    public class Tour
    {
        public long Id { get; set; }
        public string Temps { get; set; }
        public int Numero { get; set; }
        public long IdSession { get; set; }

        public List<Point> Points { get; set; }

        public Tour(string TempsArg,long IdSessionArg,int NumeroArg)
        {
            Numero = NumeroArg;
            Temps = TempsArg;
            IdSession = IdSessionArg;
        }

        public Tour(int nombre, string temps)
        {
            Numero = nombre;
            Temps = temps;
            Points = new List<Point>();
        }

        public override string ToString()
        {
            string sb = "-";

            sb+=$"Tour {Numero}: {Temps}\n";
            foreach (Point point in Points)
            {
                sb += $"\tTime: {point.Timer}, Distance: {point.Distance}\n";
                sb += $"\tNGear: {point.NGear}, PBrakeF: {point.PBrakeF}, ASteer: {point.ASteer}\n";
                sb += $"\tRPedal: {point.RPedal}, GLong: {point.GLong}, GLat: {point.GLat}\n";
                sb += $"\tVCar: {point.VCar}, Geo: {point.Latitude}, {point.Longitude}\n\n";
            }

            return sb;
        }

    }
}
