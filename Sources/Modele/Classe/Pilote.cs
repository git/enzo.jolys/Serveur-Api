﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modele.Classe
{
    public class Pilote
    {
        public string Pseudo { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public string Image { get; set; }

        public Pilote(string _Pseudo, string _Email, string _Password,string _Image)
        {
            Pseudo = _Pseudo;
            Email = _Email;
            Password = _Password;
            Image = _Image;
        }

        public Pilote() { }
    }
}
