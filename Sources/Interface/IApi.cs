﻿using Dto.Classe;
using Modele.Classe;

namespace Interface
{
    public interface IApi
    {
        

        // Pilotes
        Task<IEnumerable<Pilote>> GetPilotes();
        Task<Pilote> GetOnePilote(string pseudo);
        Task<Pilote> AddPilote(Pilote pilote);
        Task<Pilote> CheckPilote(string Email, string password);


        // Ecuries 
        Task<IEnumerable<Ecurie>> GetEcuries();
        Task<Ecurie> GetOneEcurie(string name);
        Task<List<int>> AddEcurie(Ecurie ecurie,string pseudo);

        // Circuits
        Task<IEnumerable<Circuit>> GetCircuits();
        Task<Circuit> GetOneCircuit(string name);
        Task<Circuit> AddCircuit(Circuit circuit);

        // Session
        Task<IEnumerable<Session>> GetSessionsByPilotes(string pseudo);
        Task<IEnumerable<Session>> GetFullSessions();
        Task<Session> AddSession(Session session, string namePseudo, string nameCircuit,string nameSession,string typeSession);


        // Tours 
        Task<IEnumerable<Tour>> GetTours(long IdSession);

        // Point 
        Task<IEnumerable<Point>> GetPoints(long IdTour);

        // Geolocalisation
        Task<Geolocalisation> GetGeolocalisations(long IdPoint);

        //Image 
        Task<Image> GetImage(int IdPilote);
    }
}
