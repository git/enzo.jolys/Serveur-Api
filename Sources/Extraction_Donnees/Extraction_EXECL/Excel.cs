﻿
using ExcelDataReader;
using Microsoft.AspNetCore.Http;
using Modele.Classe;
using System.Text.RegularExpressions;
using Spire.Xls;
using NPOI;
using NPOI.POIFS.FileSystem;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Data;
using System.Globalization;

namespace Extraction_Donnees.Extraction_EXECL
{
    public static class Excel
    {

        // FONCTION A UTILISER POUR RECUPERER LES DONNES EXCEL AVEC L'API
        public static async Task<Session> ReadExcel(IFormFile file)
        {
            var session = new Session();

            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            using (var stream = new MemoryStream())
            {
                file.CopyTo(stream);
                stream.Position = 0;
/*                int tourNo = -1;
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    while (reader.Read())
                    {
                        if (reader.GetValue(2) == null) break;
                        if (reader.Depth == 0) continue; // sauter les en-têtes

                        var tourExtrait = reader.GetString(0);
                        if (!string.IsNullOrEmpty(tourExtrait))
                        {
                            tourNo = Int32.Parse(Regex.Match(tourExtrait, @"\d+").Value); // récupérer le numéro du tour
                            Tour tour = new Tour(tourNo, reader.GetString(1)); // création du tour

                            session.Tours.Add(tour); // ajouter le tour sans points à la session
                        }
                        try
                        {

                            var point = new Point(
                            new Geolocalisation(reader.GetDouble(11), reader.GetDouble(12)),
                                (float)reader.GetDouble(2),
                                (float)reader.GetDouble(3),
                                (float)reader.GetDouble(4),
                                (float)reader.GetDouble(5),
                                (float)reader.GetDouble(6),
                                (float)reader.GetDouble(7),
                                (float)reader.GetDouble(8),
                                (float)reader.GetDouble(9),
                                (float)reader.GetDouble(10));
                            if (tourNo != -1)
                            {
                                session.Tours.Find(t => t.Nombre == tourNo).Points.Add(point);
                            }
                            else
                            {
                                throw new ArgumentException("Aucun tour n'a été trouvé dans le fichier !");
                            }

                        }
                        catch (IndexOutOfRangeException e)
                        {
                            throw new ArgumentException($"Problème lors de la lecture des données, données manquantes ! (colonnes manquantes dans l'Excel). {e.Message}");
                        }

                    }
                }*/
                using (var reader = new StreamReader(stream))
                {
                    int cpt = 0;
                    int tourNo = -1;
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        cpt++;
                        var values = line.Split('\t');

                        if (cpt == 1) continue;
                        if (values.Count() < 4)
                            break;
                        if (reader.Peek() == -1) break;

                        if (values[0].ToLower().Contains("lap:"))
                        {
                            tourNo = Int32.Parse(Regex.Match(values[0], @"\d+").Value); // récupérer le numéro du tour
                            Tour tour = new Tour(tourNo, values[1]); // création du tour

                            session.Tours.Add(tour); // ajouter le tour sans points à la session
                        }
                        else
                        {
                            if (cpt != 2 && cpt % 2 == 0) continue; // saute la moitié des points (optimization)
                            try
                            {
                                var point = new Point(
                                    double.Parse(values[11], CultureInfo.InvariantCulture), 
                                    double.Parse(values[12], CultureInfo.InvariantCulture),
                                    float.Parse(values[2], CultureInfo.InvariantCulture),
                                    float.Parse(values[3], CultureInfo.InvariantCulture),
                                    float.Parse(values[4], CultureInfo.InvariantCulture),
                                    float.Parse(values[5], CultureInfo.InvariantCulture),
                                    float.Parse(values[6], CultureInfo.InvariantCulture),
                                    float.Parse(values[7], CultureInfo.InvariantCulture),
                                    float.Parse(values[8], CultureInfo.InvariantCulture),
                                    float.Parse(values[9], CultureInfo.InvariantCulture),
                                    float.Parse(values[10], CultureInfo.InvariantCulture)
                                );


                                if (tourNo != -1)
                                {
                                    session.Tours.Find(t => t.Numero == tourNo).Points.Add(point);
                                }
                                else
                                {
                                    throw new ArgumentException("Aucun tour n'a été trouvé dans le fichier !");
                                }
                            }
                            catch (IndexOutOfRangeException e)
                            {
                                throw new ArgumentException($"Problème lors de la lecture des données, données manquantes ! (colonnes manquantes dans l'Excel). {e.Message}");
                            }
                        }
                    }
                }
            }

            return session;
        }

        public static async Task<Session> TestExcel(string filePath)
        {
            var session = new Session();
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                int tourNo = -1;
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    while (reader.Read())
                    {
                        if (reader.GetValue(2) == null) break;
                        if (reader.Depth == 0) continue; // sauter les en-têtes

                        var tourExtrait = reader.GetString(0);
                        if (!string.IsNullOrEmpty(tourExtrait))
                        {
                            tourNo = Int32.Parse(Regex.Match(tourExtrait, @"\d+").Value); // récupérer le numéro du tour
                            Tour tour = new Tour(tourNo, reader.GetString(1)); // création du tour

                            session.Tours.Add(tour); // ajouter le tour sans points à la session
                        }
                        try
                        {

                            var point = new Point(
                                reader.GetDouble(11),
                                reader.GetDouble(12),
                                (float)reader.GetDouble(2),
                                (float)reader.GetDouble(3),
                                (float)reader.GetDouble(4),
                                (float)reader.GetDouble(5),
                                (float)reader.GetDouble(6),
                                (float)reader.GetDouble(7),
                                (float)reader.GetDouble(8),
                                (float)reader.GetDouble(9),
                                (float)reader.GetDouble(10)) ;
                            if (tourNo != -1)
                            {
                                session.Tours.Find(t => t.Numero == tourNo).Points.Add(point);
                            }
                            else
                            {
                                throw new ArgumentException("Aucun tour n'a été trouvé dans le fichier !");
                            }

                        }
                        catch (IndexOutOfRangeException e)
                        {
                            throw new ArgumentException($"Problème lors de la lecture des données, données manquantes ! (colonnes manquantes dans l'Excel). {e.Message}");
                        }

                    }
                }
            }

            return session;
        }

        public static async Task<Session> TestExcel2(string filePath)
        {
            var session = new Session();
            using (var reader = new StreamReader(filePath))
            {
                int cpt = 0;
                int tourNo = -1;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    cpt++;
                    var values = line.Split('\t');

                    if (cpt == 1) continue;
                    if (values.Count() < 4) 
                        break;
                    if (reader.Peek() == -1) break;

                    if (values[0].ToLower().Contains("lap:"))
                    {
                        tourNo = Int32.Parse(Regex.Match(values[0], @"\d+").Value); // récupérer le numéro du tour
                        Tour tour = new Tour(tourNo, values[1]); // création du tour

                        session.Tours.Add(tour); // ajouter le tour sans points à la session
                    }
                    else
                    {
                        try
                        {

                            var point = new Point(
                               double.Parse(values[11], CultureInfo.InvariantCulture), 
                               double.Parse(values[12], CultureInfo.InvariantCulture),
                                float.Parse(values[2], CultureInfo.InvariantCulture),
                                float.Parse(values[3], CultureInfo.InvariantCulture),
                                float.Parse(values[4], CultureInfo.InvariantCulture),
                                float.Parse(values[5], CultureInfo.InvariantCulture),
                                float.Parse(values[6], CultureInfo.InvariantCulture),
                                float.Parse(values[7], CultureInfo.InvariantCulture),
                                float.Parse(values[8], CultureInfo.InvariantCulture),
                                float.Parse(values[9], CultureInfo.InvariantCulture),
                                float.Parse(values[10], CultureInfo.InvariantCulture)
                            );


                            if (tourNo != -1)
                            {
                                session.Tours.Find(t => t.Numero == tourNo).Points.Add(point);
                            }
                            else
                            {
                                throw new ArgumentException("Aucun tour n'a été trouvé dans le fichier !");
                            }
                        }
                        catch (IndexOutOfRangeException e)
                        {
                            throw new ArgumentException($"Problème lors de la lecture des données, données manquantes ! (colonnes manquantes dans l'Excel). {e.Message}");
                        }
                    }
                }
            }

            return session;
        }
        public static DataTable TextExcel3(string filePath)
        {
            DataTable dt = new DataTable();
            try
            {
                using (StreamReader sr = new StreamReader(filePath))
                {
                    // read the header line and create columns in the DataTable
                    string[] headers = sr.ReadLine().Split('\t');
                    foreach (string header in headers)
                    {
                        dt.Columns.Add(header.Trim());
                    }

                    // read the data lines and add rows to the DataTable
                    while (!sr.EndOfStream)
                    {
                        string[] values = sr.ReadLine().Split('\t');
                        DataRow row = dt.NewRow();
                        for (int i = 0; i < headers.Length; i++)
                        {
                            if (i < values.Length)
                            {
                                row[i] = values[i].Trim();
                            }
                            else
                            {
                                row[i] = "";
                            }
                        }
                        dt.Rows.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }
            return dt;
        }


        public static string OpenDIS(string path)
        {
            try
            {
                string text = File.ReadAllText(path);
                return text;

            }
            catch (IOException e)
            {
                Console.WriteLine($"Error reading file: {e.Message}");
            }
            return "l";

        }
    }

}