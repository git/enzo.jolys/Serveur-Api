﻿using Entity_Framework;
using Entity_Framework.Entity;
using Entity_Framework.Factories;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extraction_Donnees.Extraction
{
    public partial class Extraction
    {
        public Task<Image> GetImage(int IdPilote)
        {
            Images image = new Images();
            using (BDDContext db = new BDDContext())
            {
                image = db.Images.Where( e => e.IdPilote == IdPilote).FirstOrDefault();
            }
            if ( image == null)
            {
                return Task.FromResult<Image>(null);
            }
            return Task.FromResult<Image>(image.EntityToModele());
        }
    }
}
