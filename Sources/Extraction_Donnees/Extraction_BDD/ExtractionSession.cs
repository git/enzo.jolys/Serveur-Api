﻿using Entity_Framework.Entity;
using Entity_Framework;
using Modele.Classe;
using Entity_Framework.Factories;
using Dto.Factories;

namespace Extraction_Donnees.Extraction
{
    public partial class Extraction
    {
        public Task<IEnumerable<Session>> GetSessionsByPilotes(string pseudo)
        {
            IEnumerable<Sessions> result = new List<Sessions>();

            using (BDDContext db = new BDDContext())
            {
                bool checkPilote = db.Pilotes.Any(e => e.Pseudo == pseudo);
                if (checkPilote == false) 
                {
                    return Task.FromResult<IEnumerable<Session>>(null);
                }
                result = (from session in db.Sessions 
                          from pilotes in db.Pilotes
                          where pilotes.Id == session.IdPilote
                          select session).ToList();
            }
            return Task.FromResult<IEnumerable<Session>>(result.Select( e => e.EntityToModele()));
        }



        public Task<Session> AddSession(Session session,string namePseudo,string nameCircuit,string nameSession,string typeSession)
        {
            int idPilote;
            using (BDDContext db = new BDDContext())
            {
                // Check si pilote existe 
                Pilotes piloteEntity =  db.Pilotes.Where(e => e.Pseudo == namePseudo).FirstOrDefault();
                if (piloteEntity == null)
                {
                    return Task.FromResult<Session>(null);
                }

                idPilote = piloteEntity.Id;
                // Check si circuit existe
                Circuits circuitEntity = db.Circuits.Where( e => e.Name == nameCircuit).FirstOrDefault();

                Sessions sessionsEntity = session.ModeleToEntity();
                if(circuitEntity != null)
                {
                    sessionsEntity.IdCircuit = circuitEntity.Id;
                }


                sessionsEntity.IdPilote = piloteEntity.Id;
                sessionsEntity.Name = nameSession;
                sessionsEntity.Type = typeSession;
                db.Sessions.Add(sessionsEntity);
                db.SaveChanges();
            }
            Console.WriteLine("Session fini !");

            long idSession;
            IEnumerable<Tours> tours = session.Tours.Select( e => e.ModeleToEntity());
            using (BDDContext db = new BDDContext())
            {
                Sessions sessions = db.Sessions.Where(e => e.Name == nameSession && e.IdPilote == idPilote).First();
                idSession = sessions.Id;
                foreach (Tours tour in tours)
                {
                    tour.IdSession = idSession;
                    db.Add(tour);
                }
                db.SaveChanges();
            }
            Console.WriteLine("Tour fini !");

            using (BDDContext db = new BDDContext())
            {
                int i = 1;
                foreach (Tour tour in session.Tours)
                {
                    long idTour = db.Tours.Where(e => e.Numero == i && e.IdSession == idSession).First().Id;

                    foreach (Point point in tour.Points)
                    {
                        Points entityPoint = point.ModeleToEntity();
                        entityPoint.IdTours = idTour;
                        db.Points.Add(entityPoint);
                    }
                    i++;
                }
                db.SaveChanges();
            }
            Console.WriteLine("Point fini !");

            return Task.FromResult<Session>(session);
        }
    }
}
