﻿using Entity_Framework;
using Entity_Framework.Entity;
using Entity_Framework.Factories;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extraction_Donnees.Extraction
{
    public partial class Extraction
    {
        public Task<IEnumerable<Point>> GetPoints(long IdTour)
        {
            IEnumerable<Points> result = new List<Points>();

            using (BDDContext db = new BDDContext())
            {
                bool checkTour = db.Tours.Any(e => e.Id == IdTour);
                if (checkTour == false)
                {
                    return Task.FromResult<IEnumerable<Point>>(null);
                }
                result = (from tour in db.Tours
                          from point in db.Points
                          where tour.Id == point.IdTours
                          select point).ToList();
            }
            return Task.FromResult<IEnumerable<Point>>(result.Select( e => e.EntityToModele()));
        }
    }
}
