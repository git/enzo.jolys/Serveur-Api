﻿using Entity_Framework;
using Entity_Framework.Entity;
using Entity_Framework.Factories;
using Modele.Classe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extraction_Donnees.Extraction
{
    public partial class Extraction
    {
        public Task<IEnumerable<Tour>> GetTours(long IdSession)
        {
            IEnumerable<Tours> result = new List<Tours>();

            using (BDDContext db = new BDDContext())
            {
                bool checkSession = db.Sessions.Any(e => e.Id == IdSession);
                if (checkSession == false)
                {
                    return Task.FromResult<IEnumerable<Tour>>(null);
                }
                result = (from tour in db.Tours
                          from session in db.Sessions
                          where session.Id == tour.IdSession
                          select tour).ToList();
            }
            return Task.FromResult<IEnumerable<Tour>>(result.Select(e => e.EntityToModele()));
        }
    }
}
