﻿using Entity_Framework;
using Entity_Framework.Entity;
using Entity_Framework.Factories;
using Modele.Classe;
using System.Security.Cryptography;

namespace Extraction_Donnees.Extraction
{
    public partial class Extraction
    {
        public Task<IEnumerable<Circuit>> GetCircuits()
        {
            IEnumerable<Circuit> result = new List<Circuit>();

            using (BDDContext db = new BDDContext())
            {
                result = db.Circuits.ToList().Select(e => e.EntityToModele());
            }

            return Task.FromResult<IEnumerable<Circuit>>(result);
        }

        public Task<Circuit> GetOneCircuit(string name)
        {
            Circuits circuit = new Circuits();

            using (BDDContext db = new BDDContext())
            {
                circuit = db.Circuits.Where(e => e.Name == name).FirstOrDefault();
            }
            if (circuit == null)
            {
                return Task.FromResult<Circuit>(null);
            }
            return Task.FromResult<Circuit>(circuit.EntityToModele());
        }

        public Task<Circuit> AddCircuit(Circuit circuit)
        {
            Circuits circuitEntity = circuit.ModeleToEntity();

            using (BDDContext db = new BDDContext())
            {
                bool checkCircuit = db.Circuits.Any(e => e.Name == circuitEntity.Name);
                if (checkCircuit)
                {
                    return Task.FromResult<Circuit>(null);
                }
                db.Add(circuitEntity);
                db.SaveChanges();
            }

            return Task.FromResult<Circuit>(circuitEntity.EntityToModele());
        }

        
    }
}
