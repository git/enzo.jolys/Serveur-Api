﻿using Entity_Framework;
using Entity_Framework.Entity;
using Entity_Framework.Entity.Relations;
using Entity_Framework.Factories;
using Modele.Classe;
using System.Collections.Generic;

namespace Extraction_Donnees.Extraction
{
    public partial class Extraction
    {
        public Task<IEnumerable<Ecurie>> GetEcuries()
        {
            IEnumerable<Ecuries> result = new List<Ecuries>();
            IEnumerable<Ecurie> list = new List<Ecurie>();
            using (BDDContext db = new BDDContext())
            {
                result = db.Ecuries.ToList();
                foreach (Ecuries ecu in result)
                {
                    Ecurie ecurie = new Ecurie("Inconnu !");
                    ecurie.name = ecu.Name;
                    ecurie.Owner = (from pilote in db.Pilotes
                                    from relation in db.RelationEcuriePilote
                                    from ecuTMP in db.Ecuries
                                    where ecuTMP.Id == relation.IdEcurie && relation.IdPilote == pilote.Id && relation.Grade == "Owner" && ecu.Id == ecuTMP.Id
                                    select pilote).Select( e => e.EntityToModele()).First();
                    
                    ecurie.Members = (from relation in db.RelationEcuriePilote
                                      from pilotes in db.Pilotes
                                      where relation.IdPilote == pilotes.Id && relation.IdEcurie == ecu.Id && relation.Grade == "Member" 
                                      select pilotes).ToList().Select(e => e.EntityToModele()).ToList();

                    ecurie.WaitingMembers = (from relation in db.RelationEcuriePilote
                                      from pilotes in db.Pilotes
                                      where relation.IdPilote == pilotes.Id && relation.IdEcurie == ecu.Id && relation.Grade == "WaitingMember"
                                      select pilotes).ToList().Select(e => e.EntityToModele()).ToList();
                    list.ToList().Add(ecurie);
                }
            }
            return Task.FromResult<IEnumerable<Ecurie>>(list);

        }

        public Task<Ecurie> GetOneEcurie(string name)
        {
            Ecuries ecurie = new Ecuries();

            using (BDDContext db = new BDDContext())
            {
                ecurie = db.Ecuries.Where(e => e.Name == name).FirstOrDefault();
            }
            if(ecurie == null)
            {
                return Task.FromResult<Ecurie>(null);
            }
            return Task.FromResult<Ecurie>(ecurie.EntityToModele());
        }

        public Task<List<int>> AddEcurie(Ecurie ecurie, string pseudo)
        {
            // IEnumerable<int> 
            //Indice 0 : Réussi ou pas -> value : 0 ok / 1 erreur
            //Indice 1 : L'erreur -> Value : 2 nom ecurie / 3 pilote introuvable / 4 fait déjà partie d'une ecurie

            List<int> result = new List<int>();
            Ecuries ecurieEntity = ecurie.ModeleToEntity();
            Relation_Pilote_Ecurie relation = new Relation_Pilote_Ecurie();

            using (BDDContext db = new BDDContext())
            {
                bool checkEcurieName = db.Ecuries.Any(e => e.Name == ecurie.name);
                if (checkEcurieName == true)
                {
                    result.Add(1);
                    result.Add(2);
                    return Task.FromResult<List<int>>(result);
                }
                bool chackPilotePseudo = db.Pilotes.Any(e => e.Pseudo == pseudo);
                if ( chackPilotePseudo == false )
                {
                    result.Add(1);
                    result.Add(3);
                    return Task.FromResult<List<int>>(result);
                }
                int idPilote = db.Pilotes.Where(e => e.Pseudo == pseudo).First().Id;

                bool checkPiloteWithEcurie = db.Ecuries.Any( e => e.IdPiloteProprietaire == idPilote);
                if (checkPiloteWithEcurie == true )
                {
                    result.Add(1);
                    result.Add(4);
                    return Task.FromResult<List<int>>(result);
                }
                ecurieEntity.IdPiloteProprietaire = idPilote;
                relation.IdPilote = idPilote;

                db.Add(ecurieEntity);
                db.SaveChanges();
            }

            using (BDDContext db = new BDDContext())
            {
                Ecuries ecurieRelation = new Ecuries();
                int idEcurie = db.Ecuries.Where(e => e.Name == ecurie.name).First().Id;

                relation.IdEcurie = idEcurie;
                relation.Grade = "Owner";
                db.Add(ecurieRelation);
                db.SaveChanges();
            }

            result.Add(0);
            return Task.FromResult<List<int>>(result);
        }

    }
}
