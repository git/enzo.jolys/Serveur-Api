﻿using Dto.Classe;
using Entity_Framework.Entity;
using Entity_Framework;
using Interface;
using Modele.Classe;
using Entity_Framework.Factories;
using System.Text.Json.Nodes;

namespace Extraction_Donnees.Extraction

{
    public partial class Extraction : IApi
    {
        public Task<IEnumerable<Session>> GetFullSessions()
        {
            List<Session> result = new();

            using (BDDContext db = new BDDContext())
            {
               foreach(var session in db.Sessions)
               {
                    Session sesh = session.EntityToModele(); 
                    sesh.Tours = new List<Tour>();

                    foreach(var t in db.Tours.Where(t => t.IdSession == sesh.Id))
                    {
                        var ta = t.EntityToModele();
                        ta.Points = new List<Point>();
                        ta.Id = t.Id;
                        sesh.Tours.Add(ta);
                    }

                    foreach (var t in sesh.Tours)
                    {
                        foreach(var p in db.Points)
                        {
                            if(t.IdSession == t.Id)
                            {
                                t.Points.Add(p.EntityToModele());
                            }
                        }
                        foreach(var p in db.Points.Where(p => p.IdTours == t.Id))
                        {
                            t.Points.Add(p.EntityToModele());
                        }
                    }
                    result.Add(sesh);
                    
               }
            }

            return Task.FromResult( result as IEnumerable<Session> );

        }
    }
}
